#!/usr/bin/python
# -*- coding: utf-8 -*-


# Clase que representa a un estanque
class Estanque:
    # Constructor de la clase estanque
    def __init__(self):
        # El estanque tiene 32 litros
        self.estanque = 32

    # Metodo para obtener el estanque en cada momento
    def get_estanque(self):
        return self.estanque
