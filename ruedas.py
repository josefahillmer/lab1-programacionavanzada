#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# Clase que representa las ruedas
class Rueda:
    # Constructor de la clase rueda
    def __init__(self, inicio):
        self.rueda = inicio

    def set_rueda(self):
        self.rueda = inicio
    # Metodo para obtener las ruedas
    def get_rueda(self):
        return self.rueda

    # Metodo para ver el deteriodo de las ruedas
    def deteriodo(self):
        # Deteriodo aleatorio
        deteriodo = random.randrange(1, 10)
        self.rueda = self.rueda - deteriodo
        # Si las ruedas tienen un deteriodo mayor a 90 % se cambian
        if self.rueda < 10:
            print("Se cambiaron las ruedas por su deteriodo")
            self.rueda = 100

        return self.rueda
