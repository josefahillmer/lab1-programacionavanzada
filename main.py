#!/usr/bin/python
# -*- coding: utf-8 -*-


from Vehiculo import Vehiculo
import sys


# Función principal o main
if __name__ == '__main__':

    auto = "Auto jose"
    auto = Vehiculo(auto)
    auto.set_motor()
    auto.set_estanque_inicial()

    # Se imprime los datos del auto
    print("ESTADO INICIAL DEL AUTO")
    print("El motor es de {0} cilintradas".format(auto.get_motor()))
    print("El estanque es de {0} Lt".format(auto.get_estanque()))
    print("El auto esta apagado")
    print("El estado de las ruedas son: {0}% de desgaste".format(0))

    # Esta en reposo
    contador = 0
    combustible = auto.get_estanque()
    while combustible > 0:
        mover = str(input("S._ Para mover el auto\nD._ Para detenerlo:"))
        if mover.upper() == 'S':
            if contador == 0:
                auto.set_estanque()
                # Se mueve
                ON_off = True
                auto.mover(ON_off)
                # Se movio asi que el contador cambia
                contador = 10
            else:
                # Esta detenido
                ON_off = False
                auto.mover(ON_off)

        else:
            print("El auto esta detenido")
            sys.exit()

    # El auto se movio asi que gasta combustible
    combustible = auto.get_estanque()


