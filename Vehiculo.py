#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from estanque import Estanque
from motor import Motor
from ruedas import Rueda
from velocimetro import Velocimetro
import random
import sys


# Clase que representa a un vehiculo = auto
class Vehiculo():

    # Constructor de la clase vehiculo
    def __init__(self, auto):
        self.auto = auto
        self.inicio = 100
        self.motor = None
        self.cilindrada = Motor()
        self.combustible = Estanque()
        self.estanque = None
        self.rueda = Rueda(self.inicio)
        self.r1 = Rueda(self.inicio)
        self.r2 = Rueda(self.inicio)
        self.r3 = Rueda(self.inicio)
        self.r4 = Rueda(self.inicio)
        self.velocidad = Velocimetro()
        self.velocimetro = self.velocidad.get_velocidad()
        self.kilometros = 0

    # Para obtner el motor del auto
    def set_motor(self):
        self.cilindrada.set_motor()
        motor = self.cilindrada.get_motor()
        self.motor = motor

    # Para obtener el combustibel inicial
    def set_estanque_inicial(self):
        litros = self.combustible.get_estanque()
        self.estanque = litros

    def set_estanque(self):
        # Se gasta 1 % de combustible al encenderlo
        ON_off = (self.estanque * 0.01)
        self.estanque = (self.estanque - ON_off)

    # Para obtener el estanque
    def get_estanque(self):
        return self.estanque

    # Para obtener el motor
    def get_motor(self):
        return self.motor

    def mover(self, ON_off):

        if ON_off is True:
            self.velocidad.set_velocidad()
            self.velocimetro = self.velocidad.get_velocidad()
            tiempo = random.randint(1, 10)
            # formula de distancia utilizada
            distancia = self.velocimetro * tiempo
            self.kilometros = self.kilometros + distancia

            # Para generar consumo por cada tipo de motor
            if self.motor == "1.2":
                consumo = distancia / 20
                self.estanque = self.estanque - consumo
                # el combistible no puede ser negativo asi que se cambia a 0
                if self.estanque < 0:
                    self.estanque = 0
                    print("El auto no tiene combustible")

                # Se ve el deteriodo de los neumaticos
                self.r1.deterioro()
                self.r2.deterioro()
                self.r3.deterioro()
                self.r4.deterioro()
                self.datos()

            else:
                consumo = distancia / 14
                self.estanque = self.estanque - consumo
                if self.estanque < 0:
                    self.estanque = 0
                    print("El auto no tiene combustible")

                self.r1.deteriodo()
                self.r2.deteriodo()
                self.r3.deteriodo()
                self.r4.deteriodo()
                self.datos()

        elif self.estanque > 0:
            ON_off = True
            self.mover(ON_off)
        else:
            print("El auto esta detenido")
            sys.exit()

    # Se imprimen los datso del auto
    def datos(self):
        print("El auto ha recorrido {0} km".format(self.kilometros))
        print("Su velocidad es de {0} km/h".format(self.velocimetro))
        print("Su estanque tiene {0} L".format(self.estanque))
        print("El estado de las ruedas es {0} % {1} % {2} % {3} %:"
                .format(self.r1.get_rueda(),  self.r2.get_rueda(),
                self.r3.get_rueda(), self.r4.get_rueda()))

