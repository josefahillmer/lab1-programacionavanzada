#!/usr/bin/python
# -*- coding: utf-8 -*-

import random


# Clase que representa a un motor
class Motor:

    # Constructor de la clase motor
    def __init__(self):
        self.motor = None

    # Posibles cilindradas de los motores y dar valor a self.motor
    def set_motor(self):
        # Motor aleatorio
        motor = random.choice([1.2, 1.6])
        self.motor = motor

    # Metodo para obtener el motor
    def get_motor(self):
        return self.motor

