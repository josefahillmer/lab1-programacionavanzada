#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# Clase que representa a un velocimetro
class Velocimetro:
    # Constructor de la clase velocimetro
    def __init__(self):
        self.velocimetro = 0

    # Se generea la velocidad aleatoriamente
    def set_velocidad(self):
        self.velocimetro = random.randrange(10, 121)

    # Metodo para obtener el velocimetro
    def get_velocidad(self):
        return self.velocimetro
